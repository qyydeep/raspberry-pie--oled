# 基本信息导入
import time

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import subprocess

# 基本信息定义，应该用spi才用得到，I2C这些参数用不到
# Raspberry Pi pin configuration:
RST = 24     # on the PiOLED this pin isnt used这个值定义None也行
# Note the following are only used with SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

# 128x32 display with hardware I2C:
# 0.91寸OLED
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# SPI接口和分辨率的参数
# 128x64 display with hardware I2C:
# disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)

# 128x32 display with hardware SPI:
# disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST, dc=DC, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=8000000))

# 128x64 display with hardware SPI:
# disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST, dc=DC, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=8000000))

# Initialize library.
# 初始化
disp.begin()

# Clear display.
# 清屏显示
disp.clear()
disp.display()
# 此时屏幕已清空

# 取得高和宽
# 创建一个绘画的图像块
# 模式是一个像素点一个颜色
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# 得到一个绘画对象
draw = ImageDraw.Draw(image)

# 画一个黑色填充的盒子
# 参数（左，上，右，下），outline=外边框， fill=填充,[width=宽度(内边宽度)]
draw.rectangle((0, 0, width, height), outline=0, fill=0)
# draw.rectangle((0, 0, 127, 31), outline=1, fill=0, width=5)
# 此处屏幕为全黑色，其实这步可以不用做

# 开始编辑画面要显示的内容
# 首先要定义图像显示的点
# 文字定义:左,上,两个坐标为文字的原点
# 图形要定义:左，上，右，下,四个坐标为图形大小的四个点
draw.arc((0, 0, 120, 30), 0, 90, fill=255)

# 到此,以上内容运行的话是没有显示的,必须执行以下内容才会显示
# 显示图形
disp.image(image)
disp.display()
